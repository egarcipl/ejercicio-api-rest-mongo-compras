package com.techu.apitechudb.repositories;

import com.techu.apitechudb.models.ProductModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
//MongoRepository es una clase ya creada para manejar operaciones sobre mongo de tipo generico
// Esto se va a traer un productmodel en base a un string
public interface ProductRepository extends MongoRepository <ProductModel, String> {
}
