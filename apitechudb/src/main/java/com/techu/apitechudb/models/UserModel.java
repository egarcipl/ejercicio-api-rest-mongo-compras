package com.techu.apitechudb.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

//Aqui le decimos a la BBDD que tendremos una coleccion de Usuarios
@Document(collection = "Users")
public class UserModel {

    // El @Id mapea el campo que tiene justo debajo. Lo usaremos como el identificador de la bbdd
    @Id
    private String id;
    private String name;
    private int age;

    //Constructores de la clase

    public UserModel() {
    }

    public UserModel(String id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public String getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public float getAge() {
        return this.age;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }
}


