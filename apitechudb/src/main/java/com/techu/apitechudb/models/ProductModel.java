package com.techu.apitechudb.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

//Aqui le decimos a la BBDD que tendremos una coleccion de productos
@Document(collection = "products")
public class ProductModel {

    // El @Id mapea el campo que tiene justo debajo. Lo usaremos como el identificador de la bbdd
    @Id
    private String id;
    private String desc;
    private float price;

    //Constructores de la clase
    public ProductModel() {
    }

    public ProductModel(String id, String desc, float price) {
        this.id = id;
        this.desc = desc;
        this.price = price;
    }

    public String getId() {
        return this.id;
    }

    public String getDesc() {
        return this.desc;
    }

    public float getPrice() {
        return this.price;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}

