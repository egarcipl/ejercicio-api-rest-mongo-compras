package com.techu.apitechudb.controllers;


import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.services.PurchaseService;
import com.techu.apitechudb.services.PurchaseServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/apitechu/v3")
public class PurchaseController {

    @Autowired
    PurchaseService purchaseService;

    @GetMapping("/purchases")
    public ResponseEntity<List<PurchaseModel>> getPurchases(){
        System.out.println("getPurchases");

        return  new ResponseEntity<>(this.purchaseService.getPurchases(), HttpStatus.OK);

    }

    @PostMapping("/purchases")
    public ResponseEntity<PurchaseServiceResponse> addPurchase(@RequestBody PurchaseModel purchase){

        System.out.println("addPurchase");
        System.out.println("La id de la compra que se va a crear es " + purchase.getId());
        System.out.println("El usuario de la compra que se va a crear es " + purchase.getUserId());
        System.out.println("Los productos de la compra que se va a crear es " + purchase.getPurchaseItems());

        PurchaseServiceResponse result = this.purchaseService.addPurchase(purchase);

        return new ResponseEntity<>(result, result.getResponseHttpStatusCode());

    }



}
